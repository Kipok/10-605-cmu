"""
Long Short Term Memory for character level entity classification
"""
import sys
import argparse
import numpy as np
from xman import *
from utils import *
from autograd import *
from copy import deepcopy

np.random.seed(0)

class LSTM(object):
    """
    Long Short Term Memory + Feedforward layer
    Accepts maximum length of sequence, input size, number of hidden units and output size
    """
    def __init__(self, max_len, in_size, num_hid, out_size):
        self.my_xman= self._build(max_len, in_size, num_hid, out_size)

    def _build(self, max_len, in_size, num_hid, out_size):
        xm = XMan()
        Wi = f.param(name='Wi', default=np.sqrt(6.0 / (in_size + num_hid)) * \
                        np.random.uniform(-1.0, 1.0, size=(in_size, num_hid)))
        Wf = f.param(name='Wf', default=np.sqrt(6.0 / (in_size + num_hid)) * \
                        np.random.uniform(-1.0, 1.0, size=(in_size, num_hid)))
        Wo = f.param(name='Wo', default=np.sqrt(6.0 / (in_size + num_hid)) * \
                        np.random.uniform(-1.0, 1.0, size=(in_size, num_hid)))
        Wc = f.param(name='Wc', default=np.sqrt(6.0 / (in_size + num_hid)) * \
                        np.random.uniform(-1.0, 1.0, size=(in_size, num_hid)))

        Ui = f.param(name='Ui', default=np.sqrt(6.0 / (num_hid + num_hid)) * \
                        np.random.uniform(-1.0, 1.0, size=(num_hid, num_hid)))
        Uf = f.param(name='Uf', default=np.sqrt(6.0 / (num_hid + num_hid)) * \
                        np.random.uniform(-1.0, 1.0, size=(num_hid, num_hid)))
        Uo = f.param(name='Uo', default=np.sqrt(6.0 / (num_hid + num_hid)) * \
                        np.random.uniform(-1.0, 1.0, size=(num_hid, num_hid)))
        Uc = f.param(name='Uc', default=np.sqrt(6.0 / (num_hid + num_hid)) * \
                        np.random.uniform(-1.0, 1.0, size=(num_hid, num_hid)))

        bi = f.param(name='bi', default=0.1 * np.random.uniform(-1.0, 1.0, size=num_hid))
        bf = f.param(name='bf', default=0.1 * np.random.uniform(-1.0, 1.0, size=num_hid))
        bo = f.param(name='bo', default=0.1 * np.random.uniform(-1.0, 1.0, size=num_hid))
        bc = f.param(name='bc', default=0.1 * np.random.uniform(-1.0, 1.0, size=num_hid))

        W2 = f.param(name='W2', default=np.sqrt(6.0 / (num_hid + out_size)) * \
                        np.random.uniform(-1.0, 1.0, size=(num_hid, out_size)))
        b2 = f.param(name='b2', default=0.1 * np.random.uniform(-1.0, 1.0, size=out_size))

        c_start = f.input(name='c_start', default=np.zeros(num_hid))
        h_start = f.input(name='h_start', default=np.zeros(num_hid))
        y = f.input(name='y', default=np.random.rand(3, out_size))
        inputs = [None] * max_len
        for t in xrange(max_len):
            inputs[t] = f.input(name='x{}'.format(t), default=np.random.rand(3, in_size))

        h_cur = h_start
        c_cur = c_start

        for t in xrange(max_len):
            xm.__dict__['i{}'.format(t)] = f.sigmoid(f.matmul(inputs[t], Wi) + f.matmul(h_cur, Ui) + bi)
            xm.__dict__['f{}'.format(t)] = f.sigmoid(f.matmul(inputs[t], Wf) + f.matmul(h_cur, Uf) + bf)
            xm.__dict__['o{}'.format(t)] = f.sigmoid(f.matmul(inputs[t], Wo) + f.matmul(h_cur, Uo) + bo)
            xm.__dict__['ch{}'.format(t)] = f.tanh(f.matmul(inputs[t], Wc) + f.matmul(h_cur, Uc) + bc)
            xm.__dict__['c{}'.format(t)] = xm.__dict__['f{}'.format(t)] * c_cur + xm.__dict__['i{}'.format(t)] * xm.__dict__['ch{}'.format(t)]
            xm.__dict__['h{}'.format(t)] = xm.__dict__['o{}'.format(t)] * f.tanh(xm.__dict__['c{}'.format(t)])
            c_cur = xm.__dict__['c{}'.format(t)]
            h_cur = xm.__dict__['h{}'.format(t)]

        xm.o2 = f.relu(f.matmul(h_cur, W2) + b2)
        xm.output = f.softMax(xm.o2)
        xm.loss = f.avg(f.crossEnt(xm.output, y))
        return xm.setup()

def main(params, check_grads=False):
    epochs = params['epochs']
    max_len = params['max_len']
    num_hid = params['num_hid']
    batch_size = params['batch_size']
    dataset = params['dataset']
    init_lr = params['init_lr']
    output_file = params['output_file']

    # load data and preprocess
    dp = DataPreprocessor()
    data = dp.preprocess('%s.train'%dataset, '%s.valid'%dataset, '%s.test'%dataset)
    # minibatches
    mb_train = MinibatchLoader(data.training, batch_size, max_len,
           len(data.chardict), len(data.labeldict))
    mb_valid = MinibatchLoader(data.validation, len(data.validation), max_len,
           len(data.chardict), len(data.labeldict), shuffle=False)
    mb_test = MinibatchLoader(data.test, len(data.test), max_len,
           len(data.chardict), len(data.labeldict), shuffle=False)
    # build
    print "building lstm..."
    lstm = LSTM(max_len,mb_train.num_chars,num_hid,mb_train.num_labels)
    ad = Autograd(lstm.my_xman)
    wengert_list = lstm.my_xman.operationSequence(lstm.my_xman.loss)
    # perform gradients check
    if check_grads:
        value_dict = lstm.my_xman.inputDict()
        value_dict = ad.eval(wengert_list, value_dict)
        gradients = ad.bprop(wengert_list, value_dict, loss=1.0)

        names_to_check = []
        for rname in gradients.keys():
            if lstm.my_xman.isParam(rname):
                names_to_check.append(rname)
        print names_to_check

        for name in names_to_check:
            to_check = deepcopy(value_dict)
            eps = 1e-7
            grad = np.empty(value_dict[name].shape)
            for idx in np.ndindex(grad.shape):
                to_check[name][idx] += eps
                tmp = ad.eval(wengert_list, to_check)
                to_check[name][idx] -= eps
                grad[idx] = (tmp['loss'] - value_dict['loss']) / eps

            if not np.allclose(gradients[name], grad, 1e-4, 1e-4):
                print name
                print gradients[name]
                print grad
            assert(np.allclose(gradients[name], grad, 1e-4, 1e-4))
    print "done"

    print "training..."
    # get default data and params
    value_dict = lstm.my_xman.inputDict()
    lr = 0.01#init_lr
    beta1 = 0.9
    beta2 = 0.999
    m = {}
    v = {}
    eps = 1e-8
    min_val_loss = 1e9
    use_adam = True
    for i in range(epochs):
        print "Epoch: {}".format(i)
        if use_adam is False:
            for (idxs,e,l) in mb_train:
                for t in xrange(e.shape[1] - 1, -1, -1):
                    value_dict['x{}'.format(t)] = e[:, t, :]
                value_dict['y'] = l.copy()
                value_dict = ad.eval(wengert_list, value_dict)
                grads = ad.bprop(wengert_list, value_dict, loss=1.0)
                for rname, grad in grads.items():
                    if lstm.my_xman.isParam(rname):
                        value_dict[rname] -= lr * grad
        else:
            for (idxs,e,l) in mb_train:
                for t in xrange(e.shape[1] - 1, -1, -1):
                    value_dict['x{}'.format(t)] = e[:, t, :]
                value_dict['y'] = l.copy()
                value_dict = ad.eval(wengert_list, value_dict)
                grads = ad.bprop(wengert_list, value_dict, loss=1.0)
                for rname, grad in grads.items():
                    if lstm.my_xman.isParam(rname):
                        if not rname in m:
                            m[rname] = grad
                            v[rname] = m[rname] ** 2
                        value_dict[rname] -= lr * m[rname] / (1 - beta1 ** (i + 1)) / (np.sqrt(v[rname] / (1 - beta2 ** (i + 1))) + eps)
                        m[rname] = beta1 * m[rname] + (1 - beta1) * grad
                        v[rname] = beta2 * v[rname] + (1 - beta2) * grad ** 2

        # this loop is actually running only one iteration!
        for (idxs,e,l) in mb_valid:
            for t in xrange(e.shape[1] - 1, -1, -1):
                value_dict['x{}'.format(t)] = e[:, t, :]
            value_dict['y'] = l.copy()
            value_dict = ad.eval(wengert_list, value_dict)
            val_loss = value_dict['loss']
            val_acc = np.mean(np.argmax(value_dict['output'], axis=1) == np.argmax(l, axis=1))
        print "Validation loss = {}, accuracy = {}".format(val_loss, val_acc)
        if val_loss < min_val_loss:
            min_val_loss = val_loss
            optimal_params = deepcopy(value_dict)
    print "done"

    value_dict = deepcopy(optimal_params)
    for (idxs,e,l) in mb_test:
        for t in xrange(e.shape[1] - 1, -1, -1):
            value_dict['x{}'.format(t)] = e[:, t, :]
        value_dict['y'] = l.copy()
        value_dict = ad.eval(wengert_list, value_dict)
        test_loss = value_dict['loss']
        test_acc = np.mean(np.argmax(value_dict['output'], axis=1) == np.argmax(l, axis=1))
    print "Test loss = {}, accuracy = {}".format(test_loss, test_acc)
    np.save(output_file, value_dict['output'])


if __name__=='__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--max_len', dest='max_len', type=int, default=10)
    parser.add_argument('--num_hid', dest='num_hid', type=int, default=50)
    parser.add_argument('--batch_size', dest='batch_size', type=int, default=64)
    parser.add_argument('--dataset', dest='dataset', type=str, default='tiny')
    parser.add_argument('--epochs', dest='epochs', type=int, default=15)
    parser.add_argument('--init_lr', dest='init_lr', type=float, default=0.5)
    parser.add_argument('--output_file', dest='output_file', type=str, default='output')
    params = vars(parser.parse_args())
    main(params)
