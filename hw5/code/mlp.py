"""
Multilayer Perceptron for character level entity classification
"""
import argparse
import numpy as np
from xman import *
from utils import *
from autograd import *
from copy import deepcopy

np.random.seed(0)

class MLP(object):
    """
    Multilayer Perceptron
    Accepts list of layer sizes [in_size, hid_size1, hid_size2, ..., out_size]
    """
    def __init__(self, layer_sizes):
        self.my_xman = self._build(layer_sizes) # DO NOT REMOVE THIS LINE. Store the output of xman.setup() in this variable

    def _build(self, layer_sizes):
        xm = XMan()

        xm.x = f.input(name='x', default=np.random.rand(1, layer_sizes[0]))
        xm.y = f.input(name='y', default=np.random.rand(1, layer_sizes[-1]))
        xm.W1 = f.param(name='W1', default=np.sqrt(6.0 / np.sum(layer_sizes[0:2])) * \
                        np.random.uniform(-1.0, 1.0, size=(layer_sizes[0], layer_sizes[1])))
        xm.b1 = f.param(name='b1', default=0.1*np.random.uniform(-1.0, 1.0, size=layer_sizes[1]))
        xm.W2 = f.param(name='W2', default=np.sqrt(6.0 / np.sum(layer_sizes[1:3])) * \
                        np.random.uniform(-1.0, 1.0, size=(layer_sizes[1], layer_sizes[2])))
        xm.b2 = f.param(name='b2', default=0.1*np.random.uniform(-1.0, 1.0, size=layer_sizes[2]))

        xm.o1 = f.relu(f.matmul(xm.x, xm.W1) + xm.b1)
        xm.o2 = f.relu(f.matmul(xm.o1, xm.W2) + xm.b2)
        xm.output = f.softMax(xm.o2)
        xm.cross_ent = f.crossEnt(xm.output, xm.y)
        xm.loss = f.avg(xm.cross_ent)
        return xm.setup()

def main(params, check_grads=False):
    epochs = params['epochs']
    max_len = params['max_len']
    num_hid = params['num_hid']
    batch_size = params['batch_size']
    dataset = params['dataset']
    init_lr = params['init_lr']
    output_file = params['output_file']

    # load data and preprocess
    dp = DataPreprocessor()
    data = dp.preprocess('%s.train'%dataset, '%s.valid'%dataset, '%s.test'%dataset)
    # minibatches
    mb_train = MinibatchLoader(data.training, batch_size, max_len,
            len(data.chardict), len(data.labeldict))
    mb_valid = MinibatchLoader(data.validation, len(data.validation), max_len,
            len(data.chardict), len(data.labeldict), shuffle=False)
    mb_test = MinibatchLoader(data.test, len(data.test), max_len,
            len(data.chardict), len(data.labeldict), shuffle=False)

    # build
    print "building mlp..."
    mlp = MLP([max_len*mb_train.num_chars,num_hid,mb_train.num_labels])
    ad = Autograd(mlp.my_xman)
    wengert_list = mlp.my_xman.operationSequence(mlp.my_xman.loss)
    # perform gradients check
    if check_grads:
        value_dict = mlp.my_xman.inputDict()
        value_dict = ad.eval(wengert_list, value_dict)
        gradients = ad.bprop(wengert_list, value_dict, loss=1.0)

        names_to_check = ['W1', 'b1', 'W2', 'b2']
        for name in names_to_check:
            to_check = deepcopy(value_dict)
            eps = 1e-7
            grad = np.empty(value_dict[name].shape)
            for idx in np.ndindex(grad.shape):
                to_check[name][idx] += eps
                tmp = ad.eval(wengert_list, to_check)
                to_check[name][idx] -= eps
                grad[idx] = (tmp['loss'] - value_dict['loss']) / eps

            if not np.allclose(gradients[name], grad, 1e-4):
                print gradients[name], grad
            assert(np.allclose(gradients[name], grad, 1e-4))
    print "done"

    # train
    print "training..."
    # get default data and params
    value_dict = mlp.my_xman.inputDict()
    lr = init_lr
    min_val_loss = 1e9
    use_sgd = True
    if use_sgd is False:
        v = {}
        mm = 0.99
        value_dict = ad.eval(wengert_list, value_dict)
        for rname in value_dict:
            if mlp.my_xman.isParam(rname):
                v[rname] = np.array(0.0)

    for i in range(epochs):
        print "Epoch: {}".format(i)
        if (i + 1) % 5 == 0:
            lr /= 2
        if use_sgd is True:
            for (idxs,e,l) in mb_train:
                value_dict['x'] = e.reshape(e.shape[0], -1)
                value_dict['y'] = l
                value_dict = ad.eval(wengert_list, value_dict)
                grads = ad.bprop(wengert_list, value_dict, loss=1.0)
                for rname, grad in grads.items():
                    if mlp.my_xman.isParam(rname):
                        value_dict[rname] -= lr * grad
        else:
            for (idxs,e,l) in mb_train:
                value_dict['x'] = e.reshape(e.shape[0], -1)
                value_dict['y'] = l
                value_dict = ad.eval(wengert_list, value_dict)
                grads = ad.bprop(wengert_list, value_dict, loss=1.0)
                for rname, grad in grads.items():
                    if mlp.my_xman.isParam(rname):
                        v_old = v[rname].copy()
                        v[rname] = mm * v[rname] + lr * grad
                        value_dict[rname] -= (1 + mm) * v[rname] + mm * v_old

        # this loop is actually running only one iteration!
        for (idxs,e,l) in mb_valid:
            value_dict['x'] = e.reshape(e.shape[0], -1)
            value_dict['y'] = l
            value_dict = ad.eval(wengert_list, value_dict)
            val_loss = value_dict['loss']
            val_acc = np.mean(np.argmax(value_dict['output'], axis=1) == np.argmax(l, axis=1))
        print "Validation loss = {}, accuracy = {}".format(val_loss, val_acc)
        if val_loss < min_val_loss:
            min_val_loss = val_loss
            optimal_params = deepcopy(value_dict)
    print "done"

    value_dict = deepcopy(optimal_params)
    for (idxs,e,l) in mb_test:
        value_dict['x'] = e.reshape(e.shape[0], -1)
        value_dict['y'] = l
        value_dict = ad.eval(wengert_list, value_dict)
        test_loss = value_dict['loss']
        test_acc = np.mean(np.argmax(value_dict['output'], axis=1) == np.argmax(l, axis=1))
    print "Test loss = {}, accuracy = {}".format(test_loss, test_acc)
    np.save(output_file, value_dict['output'])

if __name__=='__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--max_len', dest='max_len', type=int, default=10)
    parser.add_argument('--num_hid', dest='num_hid', type=int, default=50)
    parser.add_argument('--batch_size', dest='batch_size', type=int, default=64)
    parser.add_argument('--dataset', dest='dataset', type=str, default='tiny')
    parser.add_argument('--epochs', dest='epochs', type=int, default=15)
    parser.add_argument('--init_lr', dest='init_lr', type=float, default=0.5)
    parser.add_argument('--output_file', dest='output_file', type=str, default='output')
    params = vars(parser.parse_args())
    main(params)
