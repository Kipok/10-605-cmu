# some useful functions
import numpy as np
from xman import *

class f(XManFunctions):
    @staticmethod
    def square(a):
        return XManFunctions.registerDefinedByOperator('square', a)
    @staticmethod
    def avg(a):
        return XManFunctions.registerDefinedByOperator('avg', a)
    @staticmethod
    def crossEnt(a, b):
        return XManFunctions.registerDefinedByOperator('crossEnt', a, b)
    @staticmethod
    def softMax(a):
        return XManFunctions.registerDefinedByOperator('softMax', a)
    @staticmethod
    def matmul(a, b):
        return XManFunctions.registerDefinedByOperator('matmul', a, b)
    @staticmethod
    def relu(a):
        return XManFunctions.registerDefinedByOperator('relu', a)
    @staticmethod
    def sigmoid(a):
        return XManFunctions.registerDefinedByOperator('sigmoid', a)
    @staticmethod
    def tanh(a):
        return XManFunctions.registerDefinedByOperator('tanh', a)


# the functions that autograd.eval will use to evaluate each function,
# to be called with the functions actual inputs as arguments

def _softmax(o):
    exp_o = np.exp(o - np.max(o, axis=1)[:,np.newaxis])
    return exp_o / np.sum(exp_o, axis=1)[:,np.newaxis]

EVAL_FUNS = {
    'add':      lambda x1, x2: x1 + x2,
    'subtract': lambda x1, x2: x1 - x2,
    'mul':      lambda x1, x2: x1 * x2,
    'square':   np.square,
    'avg':      lambda x: np.mean(x),
    'crossEnt': lambda p, y: -np.sum(y * np.log(p), axis=1),
    'softMax':  _softmax,
    'matmul':   lambda x, y: np.dot(x, y),
    'relu':     lambda x: np.maximum(x, 0),
    'sigmoid':  lambda x: 1.0 / (1.0 + np.exp(-x)),
    'tanh':     lambda x: np.tanh(x),
    }

# the functions that autograd.bprop will use in reverse mode
# differentiation.  BP_FUNS[f] is a list of functions df1,....,dfk
# where dfi is used in propagating errors to the i-th input xi of f.
# Specifically, dfi is called with the ordinary inputs to f, with two
# additions: the incoming error, and the output of the function, which
# was computed by autograd.eval in the eval stage.  dfi will return
# delta * df/dxi [f(x1,...,xk)]
# 
# NOTE: Autograd has an optimization where if it finds a softMax op
# followed by crossEnt op, it combines the backward pass for both. So
# you only need to implement the BP_FUNS for the combined operation 
# crossEnt-softMax below.

def _derivAdd(delta, x1):
    if delta.shape != x1.shape:
        # broadcast, sum along axis=0
        if delta.shape[1] != x1.shape[0]:
            raise ValueError("Dimension Mismatch")
        return delta.sum(axis=0) #we sum the gradients over the batch
    else:
        return delta

def _derivMul(delta, x):
    if delta.shape != x.shape:
        # broadcast, sum along axis=0
        if delta.shape[1] != x.shape[0]:
            raise ValueError("Dimension Mismatch")
        return (delta * x).sum(axis=0) #we sum the gradients over the batch
    else:
        return delta * x

def _ce_sm_grad_1(delta, out, z, y):
    sm_out = _softmax(z)
    return delta[:,np.newaxis] * (-y + sm_out * np.sum(y, axis=1)[:,np.newaxis])

def _ce_sm_grad_2(delta, out, z, y):
    sm_out = _softmax(z)
    return delta[:,np.newaxis] * np.log(sm_out)


BP_FUNS = {
    'add':              [lambda delta, out, x1, x2: _derivAdd(delta, x1), lambda delta, out, x1, x2: _derivAdd(delta, x2)],
    'subtract':         [lambda delta, out, x1, x2: _derivAdd(delta, x1), lambda delta, out, x1, x2:-_derivAdd(delta, x2)],
    'mul':              [lambda delta, out, x1, x2: _derivMul(delta, x2), lambda delta, out, x1, x2: _derivMul(delta, x1)],
    'square':           [lambda delta, out, x: delta * 2.0 * x],
    'avg':              [lambda delta, out, x: delta * np.ones(x.shape) / np.prod(x.shape)],
    'crossEnt-softMax': [_ce_sm_grad_1, _ce_sm_grad_2],
    'matmul':           [lambda delta, out, a, b: delta.dot(b.T), lambda delta, out, a, b: a.T.dot(delta)],
    'relu':             [lambda delta, out, x: delta * np.ones(x.shape) * (x > 0)],
    'sigmoid':          [lambda delta, out, x: delta * out * (1.0 - out)],
    'tanh':             [lambda delta, out, x: delta * (1.0 - out ** 2)],
    }
