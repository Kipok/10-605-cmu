import java.io.IOException;
import java.util.Arrays;
import java.util.Vector;

import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.Reducer.Context;

public class NB_train_hadoop {
	public static class Map extends Mapper<LongWritable, Text, Text, IntWritable> {

		private final static IntWritable one = new IntWritable(1);
		private Text word = new Text();

		private Vector<String> tokenizeWords(String text) {
			String[] words = text.split("\\s+");
			Vector<String> tokens = new Vector<String>();
			for (int i = 0; i < words.length; i++) {
				words[i] = words[i].replaceAll("\\W", "");
				if (words[i].length() > 0) {
					tokens.add(words[i]);
				}
			}
			return tokens;
		}
		
		private Vector<String> tokenizeLabels(String text) {
			String[] labels = text.split(",");
			return new Vector<String>(Arrays.asList(labels));
		}
		
		public void map(LongWritable key, Text value, Context context) 
				throws IOException, InterruptedException {
			String doc = value.toString();
	    	String[] split_text = doc.split("\t", 3);
			Vector<String> labels = tokenizeLabels(split_text[1]);
			Vector<String> words = tokenizeWords(split_text[2]);
			for (String label: labels) {
				word.set("Y=" + label);
				context.write(word, one);
				word.set("Y=*");
				context.write(word, one);
			}
			for (String label: labels) {
				for (String doc_word: words) {
					word.set(String.format("Y=%s,W=%s", label, doc_word));
					context.write(word, one);
					word.set(String.format("Y=%s,W=*", label));
					context.write(word, one);
				}
			}
		}
	}

	public static class Reduce extends Reducer<Text, IntWritable, Text, IntWritable> {
		public void reduce(Text key, Iterable<IntWritable> values, Context context)
				throws IOException, InterruptedException {
			int sum = 0;
			for (IntWritable val : values) {
				sum += val.get();
			}
			context.write(key, new IntWritable(sum));
		}
	}
}
