#!/usr/bin/env python
# encoding: utf-8

"""
@brief Phrase finding with spark
@param fg_year The year taken as foreground
@param f_unigrams The file containing unigrams
@param f_bigrams The file containing bigrams
@param f_stopwords The file containing stop words
@param w_info Weight of informativeness
@param w_phrase Weight of phraseness
@param n_workers Number of workers
@param n_outputs Number of top bigrams in the output
"""

import sys
from pyspark import SparkConf, SparkContext
from operator import add
from math import log


def main(argv):
    # parse args
    fg_year = argv[1]
    f_unigrams = argv[2]
    f_bigrams = argv[3]
    f_stopwords = argv[4]
    w_info = float(argv[5])
    w_phrase = float(argv[6])
    n_workers = int(argv[7])
    n_outputs = int(argv[8])

    """ configure pyspark """
    conf = SparkConf().setMaster('local[{}]'.format(n_workers))  \
                      .setAppName(argv[0])
    sc = SparkContext(conf=conf)

    with open(f_stopwords, 'r') as fin:
        stopwords = fin.read().split()

    def check_bigr(s):
        sp = s[0].split()
        return sp[0] not in stopwords and sp[1] not in stopwords

    uni_data = sc.textFile(f_unigrams).map(lambda s: s.split('\t')).filter(lambda s: s[0] not in stopwords).cache()
    bigr_data = sc.textFile(f_bigrams).map(lambda s: s.split('\t')).filter(check_bigr).cache()

    bigram_fg = bigr_data.filter(lambda s: s[1] == fg_year).map(lambda s: (s[0], int(s[2])))
    bigram_bg = bigr_data.filter(lambda s: s[1] != fg_year).map(lambda s: (s[0], int(s[2])))
    uni_fg = uni_data.filter(lambda s: s[1] == fg_year).map(lambda s: (s[0], int(s[2])))

    bigr_fg_cnt = bigram_fg.reduceByKey(add)
    bigr_bg_cnt = bigram_bg.reduceByKey(add)
    uni_fg_cnt = uni_fg.reduceByKey(add)

    total_cnt_fg = bigr_fg_cnt.map(lambda s: s[1]).reduce(add)
    total_cnt_bg = bigr_bg_cnt.map(lambda s: s[1]).reduce(add)
    uni_total_cnt = uni_fg_cnt.map(lambda s: s[1]).reduce(add)

    bigr_sz = bigr_data.count()
    uni_sz = uni_data.count()

    p_fg = bigr_fg_cnt.mapValues(lambda cnt: (cnt + 1.0) / (total_cnt_fg + bigr_sz))
    p_bg = bigr_bg_cnt.mapValues(lambda cnt: (cnt + 1.0) / (total_cnt_bg + bigr_sz))
    u_pr = uni_fg_cnt.mapValues(lambda cnt: (cnt + 1.0) / (uni_total_cnt + uni_sz))

    inform = p_fg.join(p_bg).mapValues(lambda fgbg: fgbg[0] * log(fgbg[0] / fgbg[1]))

    phrase_1 = p_fg.map(lambda s: (s[0].split()[0], (s[0], s[1]))).join(u_pr)
    phrase_2 = phrase_1.map(lambda s: (s[1][0][0].split()[1], s[1])).join(u_pr)
    phrase = phrase_2.map(lambda s: (s[1][0][0][0], s[1][0][0][1] * log(s[1][0][0][1] / (s[1][0][1] * s[1][1]))))

    score = inform.join(phrase).map(lambda inf_phr:
                                    (inf_phr[0].replace(' ', '-'), inf_phr[1][0] * w_info + inf_phr[1][1] * w_phrase))
    output = score.sortBy(lambda s: s[1], ascending=False).take(n_outputs)
    print("\n".join(["{}:{}".format(*phrase) for phrase in output]))

    """ terminate """
    sc.stop()


if __name__ == '__main__':
    main(sys.argv)

