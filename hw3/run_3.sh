#!/bin/bash
echo "D=10"
for (( i = 1; i <= 20; i++ ));
do 
	shuf abstract.small.train;
done | java -Xmx128m LR 10 0.5 0.0001 20 44925 abstract.small.test >> log_3

echo "D=100"
for (( i = 1; i <= 20; i++ ));
do 
	shuf abstract.small.train;
done | java -Xmx128m LR 100 0.5 0.0001 20 44925 abstract.small.test >> log_3

echo "D=1000"
for (( i = 1; i <= 20; i++ ));
do 
	shuf abstract.small.train;
done | java -Xmx128m LR 1000 0.5 0.0001 20 44925 abstract.small.test >> log_3

echo "D=10000"
for (( i = 1; i <= 20; i++ ));
do 
	shuf abstract.small.train;
done | java -Xmx128m LR 10000 0.5 0.0001 20 44925 abstract.small.test >> log_3

echo "D=100000"
for (( i = 1; i <= 20; i++ ));
do 
	shuf abstract.small.train;
done | java -Xmx128m LR 100000 0.5 0.0001 20 44925 abstract.small.test >> log_3

echo "D=1000000"
for (( i = 1; i <= 20; i++ ));
do 
	shuf abstract.small.train;
done | java LR 1000000 0.5 0.0001 20 44925 abstract.small.test >> log_3

