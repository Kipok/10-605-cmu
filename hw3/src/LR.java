import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Scanner;

public class LR {

	private int voc_size;
	private float lr;
	private float rc;
	private int max_iter;
	private int train_size;
	private ArrayList<double[]> params;
	private ArrayList<int[]> last_seen;
	private double[] probs;
	private double[] bias;
	private HashMap<String, Integer> labels_map;
	private HashMap<Integer, String> rlabels_map;
	private int counter;
	
	public LR(int voc_size_, float lr_, float rc_, int max_iter_, 
			int train_size_, String[] labels) {
		voc_size = voc_size_;
		lr = lr_;
		rc = rc_;
		max_iter = max_iter_;
		train_size = train_size_;
		params = new ArrayList<double[]>();
		last_seen = new ArrayList<int[]>();
		probs = new double[labels.length];
		bias = new double[labels.length];
		labels_map = new HashMap<String, Integer>();
		rlabels_map = new HashMap<Integer, String>();
		int i = 0;
		for (String label: labels) {
			rlabels_map.put(i, label);
			labels_map.put(label, i++);
			params.add(new double[voc_size]);
			last_seen.add(new int[voc_size]);
		}
		counter = 0;
	}
	
	private ArrayList<String> tokenizeWords(String text) {
		String[] words = text.split("\\s+");
		ArrayList<String> tokens = new ArrayList<String>();
		for (int i = 0; i < words.length; i++) {
			words[i] = words[i].replaceAll("\\W", "");
			if (words[i].length() > 0) {
				tokens.add(words[i]);
			}
		}
		return tokens;
	}
	
	private ArrayList<String> tokenizeLabels(String text) {
		return new ArrayList<String>(Arrays.asList(text.split(",")));
	}
	
	private int hash(String x) {
		return ((x.hashCode() % voc_size) + voc_size) % voc_size;
	}
	
	private double sigmoid(double x) {
		double overflow = 20;
		if (x > overflow) {
			return 0.9999999979388463;
		}
		if (x < -overflow) { 
			return 2.061153618190206e-09;
		}
		return 1.0 / (1.0 + Math.exp(-x));
	}
	
	private void fill_probs(int[] w_pos) {
		for (int l_pos = 0; l_pos < labels_map.size(); l_pos++) {
			double x = bias[l_pos];
			for (int pos: w_pos) {
				x += params.get(l_pos)[pos];
			}
			probs[l_pos] = sigmoid(x);
		}
	}
	
	private void update_params(ArrayList<String> words, ArrayList<String> labels, double clr) {
		int[] w_pos = new int[words.size()];
		for (int i = 0; i < words.size(); i++) {
			w_pos[i] = hash(words.get(i));
		}
		fill_probs(w_pos);
		int[] l_poss = new int[labels.size()];
		int[] ul_poss = new int[labels_map.size() - labels.size()];
		int i = 0, j = 0;
		for (String label: labels_map.keySet()) {
			int l_num = labels_map.get(label);
			if (!labels.contains(label)) {
				ul_poss[i++] = l_num;
				bias[l_num] -= clr * probs[l_num];
			} else {
				l_poss[j++] = l_num;
				bias[l_num] += clr * (1 - probs[l_num]);
			}
		}
		for (int pos: w_pos) {
			for (int l_pos: l_poss) {
				params.get(l_pos)[pos] *= Math.pow(1 - 2 * clr * rc, counter - last_seen.get(l_pos)[pos]);
				params.get(l_pos)[pos] += clr * (1 - probs[l_pos]);
				last_seen.get(l_pos)[pos] = counter;
			}
			for (int l_pos: ul_poss) {
				params.get(l_pos)[pos] *= Math.pow(1 - 2 * clr * rc, counter - last_seen.get(l_pos)[pos]);
				params.get(l_pos)[pos] -= clr * probs[l_pos];
				last_seen.get(l_pos)[pos] = counter;
			}
		}
	}
	
	private void finalize_epoch(double clr) {
		for (int l_pos = 0; l_pos < labels_map.size(); l_pos++) {
			for (int pos = 0; pos < voc_size; pos++) {
				params.get(l_pos)[pos] *= Math.pow(1 - 2 * clr * rc, counter - last_seen.get(l_pos)[pos]);
				last_seen.get(l_pos)[pos] = 0;
			}
		}
	}
	
	public void train(Scanner sc) {
		counter = 0;
		double epoch_num = 1;
		double clr = lr / (epoch_num * epoch_num);
		while(sc.hasNextLine()) {
			String line = sc.nextLine(); 
			String[] split_text = line.split("\t", 3);
			ArrayList<String> labels = tokenizeLabels(split_text[1]);
			ArrayList<String> words = tokenizeWords(split_text[2]);
			++counter;
			update_params(words, labels, clr);
			if (counter == train_size) {
				finalize_epoch(clr);
//				run_experiment();
				counter = 0;
				++epoch_num;
				clr = lr / (epoch_num * epoch_num);
			}
			if (epoch_num == max_iter + 1) {
				break;
			}
		}
	}
	
	private void predict_doc(ArrayList<String> words) {
		int[] w_pos = new int[words.size()];
		for (int i = 0; i < words.size(); i++) {
			w_pos[i] = hash(words.get(i));
		}
		fill_probs(w_pos);
		for (int l_pos = 0; l_pos < labels_map.size(); l_pos++) {
			System.out.print(rlabels_map.get(l_pos) + '\t' + probs[l_pos]);
			if (l_pos != labels_map.size() - 1) {
				System.out.print(',');
			}
		}
		System.out.print('\n');
	}

	private double compute_loss(ArrayList<String> words, ArrayList<String> labels) {
		int[] w_pos = new int[words.size()];
		for (int i = 0; i < words.size(); i++) {
			w_pos[i] = hash(words.get(i));
		}
		fill_probs(w_pos);
		double lcl = 0.0;
		for (int l_pos = 0; l_pos < labels_map.size(); l_pos++) {
			if (labels.contains(rlabels_map.get(l_pos))) {
				lcl += Math.log(probs[l_pos]);
			} else {
				lcl += Math.log(1.0 - probs[l_pos]);
			}
		}
		return lcl;
	}
	
	public void run_experiment() {
		try (BufferedReader br = new BufferedReader(new FileReader("abstract.small.train"))) {
		    String doc;
		    double loss = 0.0;
		    while ((doc = br.readLine()) != null) {
				String[] split_text = doc.split("\t", 3);
				ArrayList<String> labels = tokenizeLabels(split_text[1]);
				ArrayList<String> words = tokenizeWords(split_text[2]);
				loss += compute_loss(words, labels);
		    }
		    System.out.println(loss);
		} catch (IOException e) {
			System.err.println("Something wrong with the test file");
		}	
	}

	private double predict_doc_acc(ArrayList<String> words, ArrayList<String> labels) {
		int[] w_pos = new int[words.size()];
		for (int i = 0; i < words.size(); i++) {
			w_pos[i] = hash(words.get(i));
		}
		fill_probs(w_pos);
		double acc = 0.0;
		for (int l_pos = 0; l_pos < labels_map.size(); l_pos++) {
			if (probs[l_pos] >= 0.5) {
				acc += labels.contains(rlabels_map.get(l_pos)) ? 1 : 0;
			} else {
				acc += (!labels.contains(rlabels_map.get(l_pos))) ? 1 : 0;
			}
		}
		return acc / labels_map.size();
	}
	
	public void predict_test_acc(String file_name) {
		try (BufferedReader br = new BufferedReader(new FileReader(file_name))) {
		    String doc;
		    double acc = 0.0;
		    double num = 0.0;
		    while ((doc = br.readLine()) != null) {
				String[] split_text = doc.split("\t", 3);
				ArrayList<String> labels = tokenizeLabels(split_text[1]);
				ArrayList<String> words = tokenizeWords(split_text[2]);
				acc += predict_doc_acc(words, labels);
				++num;
		    }
		    System.out.println(acc / num);
		} catch (IOException e) {
			System.err.println("Something wrong with the test file");
		}		
	}
	
	public void predict_test(String file_name) {
		try (BufferedReader br = new BufferedReader(new FileReader(file_name))) {
		    String doc;
		    while ((doc = br.readLine()) != null) {
				String[] split_text = doc.split("\t", 3);
				ArrayList<String> words = tokenizeWords(split_text[2]);
				predict_doc(words);
		    }
		} catch (IOException e) {
			System.err.println("Something wrong with the test file");
		}
	}
		
    public static void main(String[] args) {
        if (args.length != 6) {
            System.out.println("You should provide 6 arguments!");
            return;
        }
        int voc_size = Integer.parseInt(args[0]);
        float lr = Float.parseFloat(args[1]);
        float rc = Float.parseFloat(args[2]);
        int max_iter = Integer.parseInt(args[3]);
        int train_size = Integer.parseInt(args[4]);
        String test_file = args[5];
        String[] labels = {
        	"ChemicalSubstance", "CelestialBody", "Biomolecule", "Organisation", "Work",
        	"Agent", "Event", "Person", "other", "Place", "Location", "SportsSeason",
        	"Activity", "Device", "MeanOfTransportation", "TimePeriod", "Species"
        };
        LR clf = new LR(voc_size, lr, rc, max_iter, train_size, labels);
        Scanner sc = new Scanner(System.in);
        clf.train(sc);
        clf.predict_test(test_file);
    }
}
