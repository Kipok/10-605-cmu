#!/bin/bash
echo "mu=0.0"
for (( i = 1; i <= 20; i++ ));
do 
	shuf abstract.small.train;
done | java -Xmx128m LR 10000 0.5 0.0 20 44925 abstract.small.test >> log_2

echo "mu=1e-6"
for (( i = 1; i <= 20; i++ ));
do 
	shuf abstract.small.train;
done | java -Xmx128m LR 10000 0.5 0.000001 20 44925 abstract.small.test >> log_2

echo "mu=1e-5"
for (( i = 1; i <= 20; i++ ));
do 
	shuf abstract.small.train;
done | java -Xmx128m LR 10000 0.5 0.00001 20 44925 abstract.small.test >> log_2

echo "mu=1e-4"
for (( i = 1; i <= 20; i++ ));
do 
	shuf abstract.small.train;
done | java -Xmx128m LR 10000 0.5 0.0001 20 44925 abstract.small.test >> log_2

echo "mu=1e-3"
for (( i = 1; i <= 20; i++ ));
do 
	shuf abstract.small.train;
done | java -Xmx128m LR 10000 0.5 0.001 20 44925 abstract.small.test >> log_2

echo "mu=1e-2"
for (( i = 1; i <= 20; i++ ));
do 
	shuf abstract.small.train;
done | java -Xmx128m LR 10000 0.5 0.01 20 44925 abstract.small.test >> log_2

echo "mu=1e-1"
for (( i = 1; i <= 20; i++ ));
do 
	shuf abstract.small.train;
done | java -Xmx128m LR 10000 0.5 0.1 20 44925 abstract.small.test >> log_2


echo "mu=0.2"
for (( i = 1; i <= 20; i++ ));
do 
	shuf abstract.small.train;
done | java -Xmx128m LR 10000 0.5 0.2 20 44925 abstract.small.test >> log_2

echo "mu=0.3"
for (( i = 1; i <= 20; i++ ));
do 
	shuf abstract.small.train;
done | java -Xmx128m LR 10000 0.5 0.3 20 44925 abstract.small.test >> log_2

echo "mu=0.5"
for (( i = 1; i <= 20; i++ ));
do 
	shuf abstract.small.train;
done | java -Xmx128m LR 10000 0.5 0.5 20 44925 abstract.small.test >> log_2

echo "mu=1.0"
for (( i = 1; i <= 20; i++ ));
do 
	shuf abstract.small.train;
done | java -Xmx128m LR 10000 0.5 1.0 20 44925 abstract.small.test >> log_2
