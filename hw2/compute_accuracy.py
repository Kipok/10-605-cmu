import sys
from ast import literal_eval

if __name__ == '__main__':
    output_file = sys.argv[1]
    test_file = sys.argv[2]
    with open(output_file, 'r') as f:
        out = f.readlines()
    with open(test_file, 'r') as f:
        test = f.readlines()
    d = {}
    for line in test:
        sp = line.split('\t')
        d[sp[0]] = sp[1].split(',')
    sm = 0
    nb = 0
    for line in out:
        nb += 1.0
        tup = literal_eval(line)
        if tup[1] in d[tup[0]]:
            sm += 1.0
    print(sm / nb)
