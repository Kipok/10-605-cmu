from guineapig import *
from ast import literal_eval

class NB(Planner):
    data = ReadLines('input_data') | \
           ReplaceEach(by=lambda s: literal_eval(s))
    queries = ReadLines('input_queries') | \
              ReplaceEach(by=lambda s: literal_eval(s))

    data_new = ReplaceEach(data, by=lambda (e_id, val): (e_id, [c_id for (p_id, c_id) in val])) | \
               Distinct()

    joined_data = Join(Jin(data_new, by=lambda x: '#'), Jin(queries, by=lambda x: '#'))
    answers = Group(joined_data, by=lambda ((e_id, val), (s_id, d_id)): (s_id, d_id),
                    reducingTo=ReduceTo(int, lambda accum, ((e_id, val), (s_id, d_id)):
                                                        accum + (s_id in val and d_id in val)))

if __name__ == "__main__":
    NB().main(sys.argv)
