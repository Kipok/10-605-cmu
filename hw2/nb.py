from __future__ import print_function
import sys
import re
import math
from guineapig import *

def eprint(*args, **kwargs):
    print(*args, file=sys.stderr, **kwargs)

def tokens(line):
    sp_line = line.split('\t')
    pattern = re.compile('[\W_]+')
    for label in sp_line[1].split(','):
        yield("Y{}".format(label))
        for word in sp_line[2].split():
            st_word = pattern.sub('', word.lower())
            yield ("{},{}".format(st_word, label))
            yield("{},*".format(st_word))

def split_test_doc(line):
    pattern = re.compile('[\W_]+')
    doc_id, _, words = line.split('\t')
    for word in words.split():
        yield (pattern.sub('', word.lower()), doc_id)

def reduce_to_dict_cl(accum, (word, count)):
    accum[word[1:]] = count
    return accum

def reduce_to_dict_wd(accum, (word, count)):
    label = word.split(',')[1]
    if label in accum:
        accum[label] += count
    else:
        accum[label] = count
    return accum

def reduce_to_dict(accum, (word, count)):
    accum[word.split(',')[1]] = count
    return accum

def get_predictions(accum, ((d_id, counts), (voc_size, words_inclass))):
    for label in words_inclass:
        val = 0.0
        if label in counts:
            val += math.log((counts[label] + 1.0) / (words_inclass[label] + voc_size))
        else:
            val += math.log(1.0 / (words_inclass[label] + voc_size))

        if label in accum:
            accum[label] += val
        else:
            accum[label] = val
    return accum

def final_update(((d_id, preds), (total_inclass, class_counts))):
    class_num = len(class_counts)
    mx_val = -1e9
    mx_label = ""
    for label in class_counts:
        preds[label] += math.log((class_counts[label] + 1.0) / (total_inclass +
                                                                class_num))
        if preds[label] > mx_val:
            mx_val = preds[label]
            mx_label = label
    return d_id, mx_label, mx_val

class NB(Planner):
    params = GPig.getArgvParams(required=['trainFile', 'testFile'])
    # reading and processing train data
    counts = ReadLines(params['trainFile']) | \
             Flatten(by=tokens) | \
             Group(by=lambda x: x, reducingTo=ReduceToCount(),
                   combiningTo=ReduceToCount())

    # counting values to store in memory
    classes_only = Filter(counts, by=lambda cnt: cnt[0].startswith("Y"))
    total_inclass = Group(classes_only, by=lambda x: '#',
                          reducingTo=ReduceTo(int, lambda accum,(word, count):
                                              accum + count)) | \
                    ReplaceEach(by=lambda (_, n): n)
    class_counts = Group(classes_only, by=lambda x: '#',
                         reducingTo=ReduceTo(dict, reduce_to_dict_cl)) | \
                   ReplaceEach(by=lambda (_, d): d)
    voc_size = Filter(counts, by=lambda cnt: '*' in cnt[0]) | \
               Group(by=lambda x: '#', reducingTo=ReduceToCount()) | \
               ReplaceEach(by=lambda (_, n): n)
    words_inclass = Filter(counts, by=lambda cnt: ',' in cnt[0] and '*' not in
                          cnt[0]) | \
                    Group(by=lambda x: '#', reducingTo=ReduceTo(dict, reduce_to_dict_wd)) | \
                    ReplaceEach(by=lambda (_, n): n)

    # making counts word-indexed
    word_counts = Filter(counts, by=lambda (word,count): not
                         word.startswith('Y') and '*' not in word) | \
                  Group(by=lambda (word,count): word.split(',')[0],
                        reducingTo=ReduceTo(dict, reduce_to_dict))

    # reading test data and generating test queries
    test_queries = ReadLines(params['testFile']) | \
                Flatten(by=split_test_doc)

    # answering queries
    ans_queries = Join(Jin(word_counts, by=lambda (word, cnts): word),
                       Jin(test_queries, by=lambda (word, d_id): word)) | \
                       ReplaceEach(by=lambda ((word1, cnts), (word2, d_id)):
                                            (d_id, cnts))
    # generating predictions for all classes
    ans_with_info = Augment(ans_queries, sideviews=[voc_size, words_inclass],
                         loadedBy=lambda v1, v2: (GPig.onlyRowOf(v1),
                            GPig.onlyRowOf(v2)))
    all_preds = Group(ans_with_info, by=lambda ((d_id, cnts), (v1, v2)): d_id,
                      reducingTo=ReduceTo(dict, get_predictions))

    # output final predictions
    all_preds_with_info = Augment(all_preds, sideviews=[total_inclass,
                                                        class_counts],
                                  loadedBy=lambda v1, v2: (GPig.onlyRowOf(v1),
                                                GPig.onlyRowOf(v2)))
    output = ReplaceEach(all_preds_with_info, by=final_update)

if __name__ == "__main__":
    NB().main(sys.argv)
