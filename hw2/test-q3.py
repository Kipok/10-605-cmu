from guineapig import *
import re

def flat1(tokens):
    for w in tokens:
        if w.isalpha():
            yield w

def flat2(tokens):
    for w in tokens:
        if not w.isalpha():
            yield re.sub("[^a-zA-Z]+", "", w) #regex substitution

class NB(Planner):
    data = ReadLines('data.txt') \
            | Map(by = lambda line : line.strip().split(' '))
    t1 = FlatMap(data, by = flat1)
    t2 = FlatMap(data, by = flat2)
    t3 = Join(Jin(t1), Jin(t2)) | Map(by = lambda(w1, w2) :
                                      w2)

if __name__ == "__main__":
    NB().main(sys.argv)
