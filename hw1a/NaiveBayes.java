import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.Arrays;
import java.util.Hashtable;
import java.util.Scanner;
import java.util.Vector;

class NaiveBayesClassifier {
	private Hashtable<String, Integer> class_counts;
	private int total_class_count;
	private Hashtable<String, Hashtable<String, Integer>> word_counts;
	private Hashtable<String, Integer> total_word_counts;
	private final double SMOOTHING_CONST;
	
	private void updateCounts(Vector<String> labels, Vector<String> words) {
		for (int i = 0; i < labels.size(); i++) {
			class_counts.merge(labels.get(i), 1, Integer::sum);
			++total_class_count;
		}
		for (int i = 0; i < labels.size(); i++) {
			for (int j = 0; j < words.size(); j++) {
				if (!word_counts.containsKey(labels.get(i))) {
					word_counts.put(labels.get(i), new Hashtable<String, Integer>());
				}
				word_counts.get(labels.get(i)).merge(words.get(j), 1, Integer::sum);
			}
			total_word_counts.merge(labels.get(i), words.size(), Integer::sum);
		}
	}
	
	private Vector<String> tokenizeWords(String text) {
		String[] words = text.split("\\s+");
		Vector<String> tokens = new Vector<String>();
		for (int i = 0; i < words.length; i++) {
			words[i] = words[i].replaceAll("\\W", "");
			if (words[i].length() > 0) {
				tokens.add(words[i]);
			}
		}
		return tokens;
	}
	
	private Vector<String> tokenizeLabels(String text) {
		String[] labels = text.split(",");
		String[] filtered_labels = Arrays.stream(labels)
										 .filter(s -> s.endsWith("CAT"))
										 .toArray(String[]::new);
		return new Vector<String>(Arrays.asList(filtered_labels));
	}
	
	public void fit(String file_name) {
		try (BufferedReader br = new BufferedReader(new FileReader(file_name))) {
		    String doc;
		    while ((doc = br.readLine()) != null) {
		    	String[] split_text = doc.split("\t", 2);
		    	Vector<String> labels = tokenizeLabels(split_text[0]);
		    	if (labels.isEmpty()) {
		    		continue;
		    	}
		    	updateCounts(labels, tokenizeWords(split_text[1]));
		    }
		} catch (IOException e) {
			System.err.println("Something wrong with your file");
		}
	}
	
	private class ProbPair {
		public String label;
		public double pred;
		public ProbPair(String label_, double pred_) {
			label = label_;
			pred = pred_;
		}
	}
	
	public ProbPair getPredictions(Vector<String> words) {
		int class_num = class_counts.keySet().size();		
		// TODO: this should be computed on fly?
		Hashtable<String, Integer> tmp = new Hashtable<String, Integer>();
		for (String key: word_counts.keySet()) {
			for (String word: word_counts.get(key).keySet()) {
				tmp.put(word, 1);
			}
		}
		int words_num = tmp.keySet().size();
		
		double max_prob = -1e9;
		String max_label = "";
		for (String label: class_counts.keySet()) {
			double cur_prob = Math.log(
					(class_counts.get(label) + SMOOTHING_CONST) / 
					(total_class_count + SMOOTHING_CONST * class_num));
			for (int i = 0; i < words.size(); i++) {
				if (word_counts.get(label) == null) {
					cur_prob += Math.log(1 / words_num); 
				} else {
					cur_prob += Math.log(
							(word_counts.get(label).getOrDefault(words.get(i), 0) + SMOOTHING_CONST) / 
							(total_word_counts.getOrDefault(label, 0) + SMOOTHING_CONST * words_num));
				}
			}
			if (cur_prob >= max_prob) {
				max_prob = cur_prob;
				max_label = label;
			}
		}
		return new ProbPair(max_label, max_prob);
	}

	public void predict(String file_name) {
		try (BufferedReader br = new BufferedReader(new FileReader(file_name))) {
		    String doc;
		    int total_count = 0;
		    int good_count = 0;
		    while ((doc = br.readLine()) != null) {
		    	String[] split_text = doc.split("\t", 2);
		    	Vector<String> labels = tokenizeLabels(split_text[0]);
		    	if (labels.isEmpty()) {
		    		continue;
		    	}
		    	ProbPair pred = getPredictions(tokenizeWords(split_text[1]));
		    	System.out.println(labels + "\t" + pred.label + "\t" + pred.pred);
		    	++total_count;
		    	if (labels.contains(pred.label)) {
		    		++good_count;
		    	}
		    }
		    System.out.println("Percent correct: " + good_count + "/" + total_count + 
		    		"=" + String.format("%.4f", 100.0 * good_count / total_count) + "%");
		} catch (IOException e) {
			System.err.println("Something wrong with your file");
		}
	}
	
	public void output_counts() {
		System.out.println(total_class_count);
		for (String key: class_counts.keySet()) {
			System.out.println(key + ',' + class_counts.get(key));
		}
		for (String key: total_word_counts.keySet()) {
			System.out.println(key + ',' + total_word_counts.get(key));
		}
		for (String label: word_counts.keySet()) {
			for (String key: word_counts.get(label).keySet()) {
				System.out.println(label + ',' + key + ',' + word_counts.get(label).get(key));
			}
		}
	}
	
	public NaiveBayesClassifier() {
		class_counts = new Hashtable<String, Integer>();
		total_class_count = 0;
		word_counts = new Hashtable<String, Hashtable<String, Integer>>();
		total_word_counts = new Hashtable<String, Integer>();
		SMOOTHING_CONST = 1.0;
	}
}

public class NaiveBayes {
	public static void main(String[] args) {
		if (args.length != 2) {
			System.err.println("Specify file that you want to process");
			return;
		}
		NaiveBayesClassifier clf = new NaiveBayesClassifier();
		clf.fit(args[0]);
//		clf.output_counts();
		clf.predict(args[1]);
	}
}

